import re
from unittest import result
from flask import *
import time
# from typing import Collection
import pymongo
from pymongo import collection







#初始化mongodb資料庫連線
client=pymongo.MongoClient("mongodb+srv://root:morris830114@mycluster.jnsis.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db=client.gamehours_it_system
db2=client.gamehours_it_system_assets
db3=client.gamehours_it_system_assets.assetscanborrowlist
db4=client.gamehours_it_system_members.members
db5=client.gamehours_it_system_assets.assets
db6=client.gamehours_it_system.itemnum
print("資料庫連線建立成功")


#所有資產設備資料庫 
#db2=client.gamehours_it_system_assets.assets

#已借出設備資料庫
#db=client.gamehours_it_system.itemnum

#目前可以借出的設備資料庫
#db3=client.gamehours_it_system_assets.assetscanborrowlist


#設定session密鑰
# app.secret_key="12345"

#初始化flask伺服器
app = Flask(__name__, static_folder="public", static_url_path="/")

#設定session密鑰
app.secret_key = "ddfsf/dfwhx."

#錯誤頁面路由
@app.route('/error')
def error():
     message = request.args.get("msg","發生錯誤,請聯繫it部門")
     return render_template("error.html", message = message)


#----------------------------登入相關------------------------------------------------------- 
#首頁 
@app.route('/')
def home():     
     result=db3.find({
          "item": "筆記型電腦"
     }).count()

     pcamount = result

     collection = db2.assets     
     assetscount = collection.find().count()
     return render_template("index.html" ,pcamount = pcamount, assetscount = assetscount )

#登入路由
@app.route('/signin', methods=["POST"])
def signin():
  result=db3.find({
          "item": "筆記型電腦"
     }).count()

  pcamount = result

  collection = db2.assets     
  assetscount = collection.find().count()  
  #從前端取得使用者的輸入
  email=request.form["email"]
  password=request.form["password"]     
  #和資料庫互動
  collection=db4
  #檢查信箱密碼是否正確
  result=collection.find_one ({
       "$and":[
            {"user":email},
            {"password":password}       
       ]
  })  
  #如果資料庫找不到對應資料,登入失敗
  if result==None:
       return redirect("/")
  
  #登入成功,在session中記錄會員資訊,導向首頁
  session['user']=result['user']
  if "user" in session:
          return render_template("index.html" , member = session["user"], pcamount = pcamount, assetscount = assetscount)

#登出路由
@app.route('/signout')
def signout():
   #移除session中的會員資料
   del session["user"]
   #導回首頁
   return redirect("/")

#重設密碼頁面
@app.route('/resetpwd')
def resetpwd():
     return render_template("/resetpwd.html")

#重設密碼路由
@app.route('/resetpwdup', methods=["POST"])
def resetpwdup():
     user = request.form["email"]
     password = request.form["password"]
     collection = db4
     result=collection.find_one({
      "user" : user     
     })
     if result != None:
          result=collection.update_one({
          "user":user
           }, {
          "$set":{
          "password":password
          }    
     })
          return redirect("/")
     if result == None:
          error = "請重新確認你輸入的帳號"
          return render_template("/resetpwd.html", error = error)

#首次登入頁面
@app.route('/register')
def register():
     return render_template("/register.html")

#首次登入路由
#使用者輸入帳號跟要註冊的密碼後,比對資料庫內的user,如果有找到對應的user則將該user的密碼設定為使用者輸入的密碼,如果使用者已經註冊過則顯示已註冊過
@app.route('/registerup', methods=["POST"])
def registerup():
     user = request.form["email"]
     password = request.form["password"]
     collection = db4
     result=collection.find_one({
      "user" : user     
     })     
     if result == None:
          error = "請聯繫it部門,您還沒有開通公司信箱"            
          return render_template("/register.html", error = error )
     
     # if result['user'] != None  and result['password'] != None:
     #      error1 = "您的帳號已經註冊過"
     #      return render_template("/register.html", error1 = error1 )
     
     if result['user'] != None:
          result=collection.update_one({
          "user":user
          }, {
          "$set":{
               "password": password
          }
          })
          return render_template("/register.html")






#----------------------------登入相關------------------------------------------------------- 




#----------------------------設備相關-------------------------------------------------------   

#借用表單
@app.route('/pages/equipment/borrowform')
def borrowform():
     if "user" in session:
          result=db3.find({ 
               'item': "筆記型電腦"
               
           }) 
          itemnumlist = result 
          return render_template("/pages/equipment/borrowform.html", itemnumlist=itemnumlist)
     else:
          return redirect("/")

#借出路由
@app.route('/borrowup', methods=["POST"])
def borrowup():
     
     #從前端接受使用者的借用資訊
     name = request.form["name"]
     item = request.form["item"]
     itemnum = request.form["itemnum"]
     borrowtime = request.form["borrowtime"]
     returntime = request.form["returntime"]
     itname = request.form["itname"] 
    
     #根據接收到的資產編號資訊比對資料庫
     collection = db3
     collection.update_one({
     "itemnum": itemnum
     }, {               
     "$set":{
     "status": "借用中",
     "borrowman":name,
     "borrowtime":borrowtime,
     "returntime":returntime,
     "itman": itname,
     }
     })
     return redirect("/pages/equipment/canborrow-list")


#-----------------------------------------------------------------------
     #根據接收到的資產編號資訊比對資料庫
     #collection = db.itemnum
     
         
     #檢查是否有相同資產編號在資料庫中
     #itemnumcheck = collection.find_one({
      #    "itemnum": itemnum
     #})
     #if itemnumcheck != None:
     #     return redirect("/error?msg=此資產已被借用")
          
     #檢查暱稱是否跟資料庫裡資料重複
     # checkname = collection.find_one({
     #      "name": name
     # })
     # if checkname != None:         
     #      return redirect("/error?msg=此員工之前有借用過設備,尚未歸還" )
     #把資料放進資料庫
     #collection.insert_one({
     #     "name":name,
     #     "item":item,
     #     "itemnum":itemnum,
     #     "borrowtime":borrowtime,
     #     "returntime":returntime,
     #     "itname": itname

     #})
     #使用者借走設備後將資料庫可借用清單比對資產編號並此筆刪除紀錄
     #collection2 = db2.assetscanborrowlist
     #checkreturnnum = collection2.find_one({
     #      "itemnum": itemnum
     #})
     #if checkreturnnum != None:
     #     result=collection2.delete_one({
     #     "itemnum": itemnum
     #})

     #return redirect("/pages/equipment/borrow")
#-----------------------------------------------------------------------    
    
#借出表單
@app.route('/pages/equipment/borrow')
def borrow():
     if "user" in session:
          collection = db.itemnum
          info_list = collection.find()  
          return render_template("/pages/equipment/borrow.html", info_list=info_list)
     else:
          return redirect("/")

#歸還表單
@app.route('/pages/equipment/itemreturn')
def itemreturn():
     if "user" in session:
          return render_template("/pages/equipment/itemreturn.html")
     else:
          return redirect("/")
#歸還路由
@app.route('/returnup', methods=["POST"])
def returnup():
     if "user" in session:
          itemnum = request.form["itemnum"]
          collection = db3
          
          collection.update_one({
          "itemnum": itemnum
          }, {               
          "$set":{
          "status": "可借用",
          "borrowman":"",
          "borrowtime":"",
          "returntime":"",
          "itman": ""
          }
          })
          checkreturnnum = collection.find_one({
           "itemnum": itemnum
          })
          if checkreturnnum == None:    
                    error = "請重新確認輸入的歸還項目資產編號"  
                    return render_template("/pages/equipment/itemreturn.html", error = error)
          else:     
                    return redirect("/pages/equipment/canborrow-list")
               
     

     # else:
     #      return redirect("/")
           
             
         
          # checkreturnnum = collection.find_one({
          #  "itemnum": itemnum
          # })
          # if checkreturnnum != None:
          #    result = collection.delete_one({
          #    "itemnum":itemnum 
          # })   
          #    return redirect("/pages/equipment/borrow")          
          # else:
          #    error = "請重新確認輸入的歸還項目資產編號"  
          #    return render_template("/pages/equipment/itemreturn.html", error = error)



     
#可借出設備清單
@app.route('/pages/equipment/canborrow-list')
def canborrowlist():    
     if "user" in session:
          collection = db2.assetscanborrowlist
          info_list = collection.find()  
          return render_template("/pages/equipment/canborrow-list.html", info_list=info_list)
     else: 
          return redirect("/")


#新增可借出設備清單
@app.route('/pages/equipment/add-canborrow-list')
def addcanborrowlist():    
     if "user" in session: 
          return render_template("/pages/equipment/add-canborrow-list.html" )
     else: 
          return redirect("/")

#新增可借出設備清單路由
@app.route('/add-canborrow-listup', methods=["POST"])
def addcanborrowlistup():    
     if "user" in session:
          manageteam = request.form["manageteam"]
          manageman = request.form["manageman"]
          itemnum = request.form["itemnum"]
          item = request.form["item"]
          brand = request.form["brand"]
          itemmodel = request.form["itemmodel"]
          RAM = request.form["RAM"]
          installoffice = request.form["installoffice"]
          OS = request.form["OS"]
          disk = request.form["disk"]
          
          collection = db3        
          result=collection.insert_one({
          "manageteam":manageteam,
          "manageman":manageman,
          "itemnum":itemnum,
          "item":item,
          "brand":brand,
          "itemmodel": itemmodel,
          "RAM":RAM,
          "installoffice":installoffice,
          "OS":OS,
          "disk":disk,
          "borrowman":"",
          "borrowtime":"",
          "itman":"",
          "returntime":"",
          "status":"可借用"
     })
          return redirect("/pages/equipment/canborrow-list")
     else: 
          return redirect("/")




#----------------------------設備相關-------------------------------------------------------     





#----------------------------資產相關-------------------------------------------------------   

#資產清單路由
@app.route('/pages/assets/assets-list')
def assets():                     
     if "user" in session:
          #秀出資料庫裡所有資產清單
          collection = db2.assets
          assets_list = collection.find()
          assetscount = collection.find().count()
          return render_template("/pages/assets/assets-list.html" , assets_list=assets_list, assetscount=assetscount)
     else:
          return redirect("/")


#資產盤點頁面路由
@app.route('/pages/assets/assets-count')
def assetscount():
    if "user" in session: 
          collection = db2.assets
          notcountassets_list = collection.find()
          # notcountassets_list = collection.find({
          #      "firstcount": "None",
          #      "secondcount": "None"           
          #      })
          notcountassetscount = notcountassets_list.count()  
          return render_template("/pages/assets/assets-count.html" , notcountassets_list=notcountassets_list, notcountassetscount=notcountassetscount )
    else:
          return redirect("/")

#資產初盤點路由
@app.route('/assets-countup', methods=["POST"])
def assetscountup():
     itemnum = request.form["itemnum"]
     collection = db2.assets
     assetscount= collection.find({
          "itemnum": itemnum
     })
     if assetscount != None:          
          result=collection.update_one({
          "itemnum":itemnum
          }, {
          "$set":{
               "firstcount": "ok"
          }
          })
     return redirect("/pages/assets/assets-count")

#資產複盤點路由
@app.route('/assets-secondcountup', methods=["POST"])
def secondassetscountup():
     itemnum = request.form["itemnum"]
     collection = db2.assets
     assetscount= collection.find({
          "itemnum": itemnum
     })
     if assetscount != None:          
          result=collection.update_one({
          "itemnum":itemnum
          }, {
          "$set":{
               "secondcount": "ok"
          }
          })
     return redirect("/pages/assets/assets-count")

#資產初盤點未點到清單
@app.route('/pages/assets/firstcount-result')
def firstcountresult():
     if "user" in session:
          #秀出資料庫裡所有初盤未點到的資產清單
          collection = db2.assets
          firstcountresult_list = collection.find({
                "firstcount": "None"         
                })
          return render_template("/pages/assets/firstcount-result.html" , firstcountresult_list=firstcountresult_list)
     else:
          return redirect("/")

#資產複盤點未點到清單
@app.route('/pages/assets/secondcount-result')
def secondcountresult():
     if "user" in session:
          #秀出資料庫裡所有初盤未點到的資產清單
          collection = db2.assets
          secondcountresult_list = collection.find({
                "secondcount": "None"         
                })
          return render_template("/pages/assets/secondcount-result.html" , secondcountresult_list=secondcountresult_list)
     else:
          return redirect("/")

#復原初盤點表格
@app.route('/pages/assets/reset-assetscount-list', methods=["POST"])
def resetassets():
     if "user" in session:
          collection = db5
          result=collection.update_many({
          "firstcount":"ok"
          }, {
          "$set":{
               "firstcount":"None"
          }
          })      

     return redirect("/pages/assets/assets-count")

#復原複盤點表格
@app.route('/pages/assets/reset-assetssecondcount-list', methods=["POST"])
def resetassetssecond():
     if "user" in session:
          collection = db5
          result=collection.update_many({
          "secondcount":"ok"
          }, {
          "$set":{
               "secondcount":"None"
          }
          })      

     return redirect("/pages/assets/assets-count")






#新增資產進資產清單
@app.route('/pages/assets/add-assets')
def addassets():
     if "user" in session:           
          return render_template("/pages/assets/add-assets.html")     
     else:
          return redirect("/")

#新增資產清單路由
@app.route('/add-assetsup', methods=["POST"])
def addassetsup():
     manageteam =  request.form["manageteam"]
     manageman = request.form["manageman"]
     itemnum = request.form["itemnum"]
     item = request.form["item"]
     brand = request.form["brand"]
     itemmodel = request.form["itemmodel"]
     buydate = request.form["buydate"]
     quantity = request.form["quantity"]
     unit = request.form["unit"]
     place = request.form["place"]
     remark = request.form["remark"]
     collection = db2.assets
     
     #比對新增的資產編號是否已存在資產清單資料庫內,如果不在則新增資料進資料庫
     itemnumcheck = collection.find_one({
          "itemnum": itemnum
     })
     
     if itemnumcheck != None:
       return redirect("/error?msg=此資產編號已存在,請重新確認")   
          
     else:
          result = collection.insert_one({
          "manageteam":manageteam,
          "manageman":manageman,
          "itemnum":itemnum,
          "item":item,
          "brand":brand,
          "itemmodel": itemmodel,
          "buydate":buydate,
          "quantity":quantity,
          "unit":unit,
          "place":place,
          "remark":remark,
          "firstcount":"None",
          "secondcount":"None"
     })          
     return redirect("/pages/assets/assets-list")

#報廢清單
@app.route('/pages/assets/scrapped-assets')
def scrappedassets():
       if "user" in session:    
          return render_template("/pages/assets/scrapped-assets.html")
       else:
          return redirect("/")
          
     
     # return render_template("/pages/assets/scrapped-assets.html")


#報廢路由
@app.route('/scrapped-assetsup', methods=["POST"])
def scrappedassetsup():
     
     collection = db2.assets
     itemnum = request.form["itemnum"]
     itemnumcheck = collection.find_one({
          "itemnum": itemnum
     })

     if itemnumcheck != None:
       result=collection.delete_one({
          "itemnum": itemnum
     })
     else:
       return redirect("/error?msg=無此資產編號,請重新確認")   
     
     return redirect("/")     

#----------------------------資產相關-------------------------------------------------------



#----------------------------Devops相關-------------------------------------------------------





#Linux常用指令
# @app.route('/pages/devops-command-line/linux')
# def linux():
#        if "user" in session:    
#           return render_template("/pages/devops-command-line/linux.html")
#        else:
#           return redirect("/")










#----------------------------Devops相關-------------------------------------------------------

#----------------------------上傳檔案相關-------------------------------------------------------




#上傳檔案頁面
# @app.route('/upload-file')
# def uploadfile():
#      return render_template("/upload-file.html")



#上傳檔案路由
# @app.route('/uploadup', methods=["POST"])
















#----------------------------上傳檔案相關-------------------------------------------------------








#----------------------------測試路由-------------------------------------------------------
#新增可借用清單
# @app.route('/test')
# def test():
#         collection = db2.assetscanborrowlist
#         collection.insert_one ({
#              "manageteam":"IT",
#              "manageman":"morris",
#              "itemnum":"1TP110193",
#              "item":"桌機",
#              "brand":"ASUS",
#              "itemmodel": "test",
#              "RAM":"8G",
#              "installoffice": "no",
#              "OS":"win10",
#              "SSD/HDD":"SSD"
#         }) 
     
#         return redirect("/")





#----------------------------測試路由-------------------------------------------------------




#啟動伺服器
app.run(port=3000 ,debug = True)

#正式
# app.run(host="0.0.0.0", port=80)









#----------------------開發測試----------------------------------------#

#測試下拉式借用選單路由
# @app.route('/test')
# def test():
#      return render_template("test.html")


# @app.route('/testup', methods=["POST"])
# def testup():
     #從前端接受使用者的借用資訊

     # item = request.form["item"]
     # itemnum = request.form["itemnum"]
     #根據接收到的資產編號資訊比對資料庫
     # collection = db.itemnum
     
         
     #檢查是否有相同資產編號在資料庫中
     # itemnumcheck = collection.find_one({
     #      "itemnum": itemnum
     # })
     # if itemnumcheck != None:
     #      return redirect("/error?msg=此資產已被借用")
          
     #檢查暱稱是否跟資料庫裡資料重複
     # checkname = collection.find_one({
     #      "name": name
     # })
     # if checkname != None:         
     #      return redirect("/error?msg=此員工之前有借用過設備,尚未歸還" )
     

     #把資料放進資料庫
     # collection.insert_one({
     #      "item":item,
     # })
     
     
     
     # return redirect("/") 

#歸還表單
# @app.route('/returntest')
# def returntest():
#      return render_template("returntest.html")

#歸還路由
# @app.route('/returntestup', methods=["POST"])
# def returntestup():
#      #從前端接收使用者填寫的歸還資訊
#      name = request.form["name"]
#      item = request.form["item"]
#      itemnum = request.form["itemnum"]
#      borrowtime = request.form["borrowtime"]
#      returntime = request.form["returntime"]
#      itname = request.form["itname"]
     
     #根據接收到的資產編號資訊比對資料庫
     # collection = db.itemnum

     #檢查是否有相同資產編號在資料庫中
     # itemnumcheck = collection.find_one({
     #      "itemnum": itemnum
     # })
     #如果資料庫中的資產編號有對應的資料,則刪除這筆借用資訊
     # if itemnumcheck != None:
     #    result=collection.delete_many({          
          # "itemnum": itemnum,    #刪除此筆資產編號在資料庫的所有紀錄          
     # })
     #如果資料庫中沒有對應的資產編號紀錄,則會將使用者導向錯誤頁面,如果有,則刪除借用紀錄並導回首頁
     # else:
     #      return redirect("/error?msg=查無此筆借用紀錄,請重新確認")
     # return redirect("/")

#借用表單
# @app.route('/borrowformtest')
# def borrowformtest():
#      collection = db2.assetscanborrowlist    
#      info_list = collection.find()  
#      return render_template("borrowformtest.html", info_list = info_list)


#借出路由
# @app.route('/borrowuptest', methods=["POST"])
# def borrowuptest():
     
#      #從前端接受使用者的借用資訊
#      name = request.form["name"]
#      item = request.form["item"]
#      itemnum = request.form["itemnum"]
#      borrowtime = request.form["borrowtime"]
#      returntime = request.form["returntime"]
#      itname = request.form["itname"]

     #根據接收到的資產編號資訊比對資料庫
     # collection = db.itemnum
     
         
     #檢查是否有相同資產編號在資料庫中
     # itemnumcheck = collection.find_one({
     #      "itemnum": itemnum
     # })
     # if itemnumcheck != None:
     #      return redirect("/error?msg=此資產已被借用")
     #把資料放進資料庫
     # collection.insert_one({
     #      "name":name,
     #      "item":item,
     #      "itemnum":itemnum,
     #      "borrowtime":borrowtime,
     #      "returntime":returntime,
     #      "itname": itname

     # })
     #使用者借走設備後將資料庫可借用清單比對資產編號並此筆刪除紀錄
     # collection2 = db2.assetscanborrowlist
     # checkreturnnum = collection2.find_one({
     #       "itemnum": itemnum
     # })
     # if checkreturnnum != None:
     #      result=collection2.delete_one({
     #      "itemnum": itemnum
     # })

     # return redirect("/")






#----------------------開發測試----------------------------------------#










          