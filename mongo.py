from typing import Collection
import pymongo

#連線雲端資料庫
client=pymongo.MongoClient("mongodb+srv://root:morris830114@mycluster.jnsis.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")

db=client.morriscoffee #選擇要操作的資料庫
collection=db.members #選擇要操作的集合


#-------------------find--------------------------
# $and(條件必須都成立)
# doc=collection.find_one({
#     "$and":[
#         {"email":"test1@gmail.com"},
#         {"password":"12345"}
#     ]
# })
# print(doc)


# $or(任一條件成立即可)
# cusor=collection.find({
#     "$or":[
#         {"level":0},
#         {"email":"test1@gmail.com"}
#     ]
# }, sort=[
#     ("level", pymongo.ASCENDING)

# ])

#用for迴圈逐一取得文件
# for doc in cusor:
#     print(doc)


#篩選一筆文件資料
# doc=collection.find_one({
#     "email":"test1@gmail.com"

# })
# print("取得的名字資料", doc["name"]) #可用["key"]取得資料


#篩選多筆文件資料
# cusor=collection.find({
#     "level":4

# })

# print(cusor)

# #用for迴圈逐一取得文件
# for doc in cusor:
#     print(doc)


# 資料排序 sort {}代表所有的資料都會進行排序
# cusor=collection.find({},sort=[
#     ("level",pymongo.ASCENDING) #ASCENDING由小到大排序 #DESCENDING由大到小排序
    
# ])
# for doc in cusor:
#     print(doc)

#-------------------find--------------------------


#-------------------delete--------------------------

#刪除一筆集合中的資料,刪除整個文件
# result=collection.delete_one({
#     "email":"test1@gmail.com" #刪除的篩選條件
# })



#刪除多筆集合中的資料

# result=collection.delete_many({
#     "level":4 #刪除所有level是4的文件
# })

# print("實際完成刪除的文件數量" ,result.deleted_count)

#-------------------delete--------------------------


#-------------------update--------------------------
#$set $unset $inc(加或減) $mul(乘) 

#更新一筆集合裡的資料
# result=collection.update_one({
#     "email":"test1@gmail.com"
# }, {
#     "$mul":{
#         "level":2
#     }
# })

#更新多筆集合裡的資料
# result=collection.update_many({
#     "level":2
# }, {
#     "$set":{
#         "level":4
#     }

# })

# print("符合條件的文件數量" ,result.matched_count)
# print("實際更新的文件數量", result.modified_count)

#-------------------update--------------------------



#-------------------insert--------------------------
#將資料存進集合中,取得資料的編號,存進result變數裡
# result=collection.insert_one({
#     "name":"test",
#     "email":"test@gmail.com",
#     "password":"1234",
#     "level":"2"

# })

#新增多筆資料進集合,取得多筆資料的編號
# result=collection.insert_many([{
#     "name":"cena",
#     "email":"test1@gmail.com",
#     "password":"12345",
#     "level":4

# }, {
#     "name":"bigshow",
#     "email":"test2@gmail.com",
#     "password":"123456",
#     "level":4
# }])


# print("資料新增成功")
# print(result.inserted_ids)

#-------------------insert--------------------------


